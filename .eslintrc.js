module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:prettier/recommended',
    'prettier',
  ],
  plugins: ['@typescript-eslint'],
  env: { node: true, es6: true },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    sourceType: 'module',
    project: './tsconfig.json',
  },
  rules: {
    // prettierのレベル
    'prettier/prettier': 'warn',
    // interfaceはI始まりでなくて良い
    '@typescript-eslint/interface-name-prefix': 'off',
    // 関数の戻り定義
    '@typescript-eslint/explicit-function-return-type': 'warn',
    // anyの利用
    '@typescript-eslint/no-explicit-any': 'warn',
    // anyの利用（上と重複するように見えるのでOFF）
    '@typescript-eslint/explicit-module-boundary-types': 'off',
  },
}
