module.exports = {
  verbose: true,
  moduleFileExtensions: ['ts', 'js'],
  testPathIgnorePatterns: ['node_modules', 'src/__tests__/_common'],
  coverageThreshold: {
    global: {
      statements: 80,
      branches: 60,
      functions: 80,
      lines: 80,
    },
  },
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  globals: {
    'ts-jest': {
      compiler: 'ttypescript',
      tsconfig: 'tsconfig.json',
      diagnostics: true,
    },
  },
  globalSetup: './src/__tests__/_common/globalSetup.ts',
}
