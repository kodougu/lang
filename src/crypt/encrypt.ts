import _crypto from 'crypto'
import { loadConfig } from '@/crypt/config'

const ALGORITHM = 'aes-256-gcm'

export function encrypt(text: string, category = ''): string {
  const config = loadConfig(category)
  if (!config.key) {
    throw new Error('config.key or config.iv is required')
  }
  // random initialization vector
  const iv = _crypto.randomBytes(16)
  // random salt
  const salt = _crypto.randomBytes(64)
  // derive encryption key: 32 byte key length
  // in assumption the masterkey is a cryptographic and NOT a password there is no need for
  // a large number of iterations. It may can replaced by HKDF
  // the value of 2145 is randomly chosen!
  const key = _crypto.pbkdf2Sync(config.key, salt, 2145, 32, 'sha512')
  // AES 256 GCM Mode
  const cipher = _crypto.createCipheriv(ALGORITHM, key, iv)
  // encrypt the given text
  const encrypted = Buffer.concat([cipher.update(text, 'utf8'), cipher.final()])
  // extract the auth tag
  const tag = cipher.getAuthTag()
  // generate output
  return Buffer.concat([salt, iv, tag, encrypted]).toString('base64')
}

export function decrypt(str: string, category = ''): string {
  const config = loadConfig(category)
  if (!config.key) {
    throw new Error('config.key or config.iv is required')
  }
  // base64 decoding
  const bData = Buffer.from(str, 'base64')
  // convert data to buffers
  const salt = bData.slice(0, 64)
  const iv = bData.slice(64, 80)
  const tag = bData.slice(80, 96)
  const text = bData.slice(96)
  // derive key using; 32 byte key length
  const key = _crypto.pbkdf2Sync(config.key, salt, 2145, 32, 'sha512')
  // AES 256 GCM Mode
  const decipher = _crypto.createDecipheriv(ALGORITHM, key, iv)
  decipher.setAuthTag(tag)
  // encrypt the given text
  let decryptedData = decipher.update(text)
  decryptedData = Buffer.concat([decryptedData, decipher.final()])
  return decryptedData.toString('utf-8')
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function encryptProperty(targetProperty: string, obj: any, category = ''): void {
  if (obj && obj[targetProperty]) {
    obj[targetProperty] = encrypt(obj[targetProperty], category)
  }
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function decryptProperty(targetProperty: string, obj: any, category = ''): void {
  if (obj && obj[targetProperty]) {
    obj[targetProperty] = decrypt(obj[targetProperty], category)
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function encryptProperties(targetProperties: string[], obj: any, category = ''): void {
  targetProperties.forEach((targetProperty) => {
    if (obj && obj[targetProperty]) {
      obj[targetProperty] = encrypt(obj[targetProperty], category)
    }
  })
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function decryptProperties(targetProperties: string[], obj: any, category = ''): void {
  targetProperties.forEach((targetProperty) => {
    if (obj && obj[targetProperty]) {
      obj[targetProperty] = decrypt(obj[targetProperty], category)
    }
  })
}
