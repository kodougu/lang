import bcrypt from 'bcrypt'
import crypto from 'crypto'
import { loadConfig } from '@/crypt/config'

/**
 * bcryptを使ったハッシュ値を生成。
 * @param password
 */
export function hash(password: string, category = ''): string {
  const config = loadConfig(category)
  return bcrypt.hashSync(password, config.saltRounds)
}
/**
 * bcryptを使ったハッシュ結果とパスワードの比較。
 * @param password
 * @param hashString
 */
export function compare(password: string, hashString: string): boolean {
  return bcrypt.compareSync(password, hashString)
}

/**
 * キーに使えるハッシュ値を生成。
 *
 * bcryptで生成したハッシュ値は、生成する都度変化する。
 * それはID/パスワードの関係であれば問題ないが、ハッシュ値そのものをキーにする場合は都合が悪い。
 * そこで、毎回同じハッシュ値になるようなハッシュ関数を用意する。
 * 少し謎な実装にしておいたほうが、推測が困難になるかな（？）と思い謎な実装にしてみた
 * @param password
 */
export function hashKey(password: string, category = ''): string {
  const config = loadConfig(category)
  if (!config.hashSalt) {
    throw new Error('CRYPTO_HASH_SALT is required.')
  }
  let hashed = crypto.createHmac('rmd160', config.hashSalt).update(password).digest('hex')
  hashed = crypto.createHmac('sha256', config.hashSalt).update(hashed).digest('hex')
  hashed = crypto.createHmac('sha384', config.hashSalt).update(hashed).digest('hex')

  for (let i = 0; i < config.saltRounds; i++) {
    hashed = crypto.createHmac('sha512', config.hashSalt).update(hashed).digest('hex')
  }
  return hashed
}
