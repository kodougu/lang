import * as dotenv from 'dotenv'
dotenv.config()

import process from 'process'

function genEnvKey(key: string, category: string): string {
  if (category) {
    return `CRYPTO_${category}_${key}`
  } else {
    return `CRYPTO_${key}`
  }
}

type CryptoConfig = { key: string; saltRounds: number; hashSalt: string }

export function loadConfig(category: string): CryptoConfig {
  const crypto_key = genEnvKey('KEY', category)
  const crypto_salt_rounds = genEnvKey('SALT_ROUNDS', category)
  const crypto_hash_salt = genEnvKey('HASH_SALT', category)

  const key: string = process.env[crypto_key] ?? ''
  const saltRounds: number = parseInt(process.env[crypto_salt_rounds] ?? '8', 10)
  const hashSalt = process.env[crypto_hash_salt] ?? ''

  return {
    key,
    saltRounds,
    hashSalt,
  }
}
