import * as dotenv from 'dotenv'
dotenv.config()

export * from '@/lang'
export * from '@/crypt'

export { logger, getLogger, enhance } from '@/logger'
