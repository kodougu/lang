import {
  encrypt,
  decrypt,
  encryptProperties,
  decryptProperties,
  encryptProperty,
  decryptProperty,
  hash,
  compare,
  hashKey,
} from '@/index'

describe('cipher', () => {
  test('cipher', () => {
    const str = 'hoge 日本語 @!~'
    const encoded = encrypt(str)
    expect(encoded).not.toBe(str)
    const decoded = decrypt(encoded)
    expect(decoded).toBe(str)
  })
  test('cipher property', () => {
    const str = 'hoge 日本語 @!~'
    const obj = {
      test: str,
      test2: str,
    }
    encryptProperty('test', obj)
    expect(obj.test).not.toBe(str)
    expect(obj.test2).toBe(str)
    decryptProperty('test', obj)
    expect(obj.test).toBe(str)
    expect(obj.test2).toBe(str)
  })
  test('cipher properties', () => {
    const str = 'hoge 日本語 @!~'
    const obj = {
      test: str,
      test2: str,
      test3: str,
    }
    encryptProperties(['test', 'test3'], obj)
    expect(obj.test).not.toBe(str)
    expect(obj.test2).toBe(str)
    expect(obj.test3).not.toBe(str)
    decryptProperties(['test', 'test3'], obj)
    expect(obj.test).toBe(str)
    expect(obj.test2).toBe(str)
    expect(obj.test3).toBe(str)
  })
  test('hash', () => {
    const hash1 = hash('test')
    expect(hash1).not.toBe('test')
    const hash2 = hash('test')
    expect(hash2).not.toBe(hash1) // bcrypt.hashの場合、生成されるハッシュ値は毎回異なる
  })
  test('compare hash', () => {
    const hash1 = hash('test')
    expect(compare('test', hash1)).toBeTruthy()
  })
  test('hashKey', () => {
    const hash1 = hashKey('test')
    expect(hash1).not.toBe('test')
    const hash2 = hashKey('test')
    expect(hash2).toBe(hash1)
    console.log(hash2)
  })
})
