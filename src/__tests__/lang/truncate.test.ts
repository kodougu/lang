import { truncate } from '@/lang/truncate'

describe('truncate', () => {
  test('truncate', () => {
    const result = truncate('1234567890', 5, '...')
    expect(result).toBe('12345...')
  })
  test('truncate', () => {
    const result = truncate('1234567890', 10, '...')
    expect(result).toBe('1234567890')
  })
})
