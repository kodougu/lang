import { kebab } from '@/index'

describe('kebab', () => {
  test('kebab', () => {
    const result = kebab('AniLInsh')
    expect(result).toBe('ani-linsh')
  })
})
