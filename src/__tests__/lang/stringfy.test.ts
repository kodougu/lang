import { stringify, decycle } from '@/index'
import { logger } from '@/logger'

describe('stringify', () => {
  test('no error', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const a: any = [{}]
    a[0].a = a
    a.push(a)
    a.push('a')
    logger.log(stringify(a))
  })
  test('no error - 2', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const object: any = {}
    object.arr = [object, object]
    object.arr.push(object.arr)
    object.obj = object
    logger.log(stringify(object, '  '))
  })
  test('no error - decycle', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const object: any = {}
    object.arr = [object, object]
    object.arr.push(object.arr)
    object.obj = object
    logger.log(decycle(object))
  })
})
