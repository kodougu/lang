import { mask, randomString } from '@/index'

describe('mask', () => {
  test('mask', () => {
    const masked = mask(randomString(10))
    expect(masked).toBe('**********')
  })
})
