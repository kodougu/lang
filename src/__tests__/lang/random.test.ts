import { randomElement, randomString } from '@/index'
import { randomElements, randomInteger } from '@/lang/random'

describe('random', () => {
  test('randomElement', () => {
    const result = randomElement(['1', '2', '3'])
    let check = false
    if (result.includes('1') || result.includes('2') || result.includes('3')) {
      check = true
    }
    expect(check).toBeTruthy()
  })
  test('randomString', () => {
    const r = randomString()
    expect(r).not.toBeNull()
    expect(r.length).toBe(4)
  })
  test('randomString - length', () => {
    const r = randomString(16)
    expect(r).not.toBeNull()
    expect(r.length).toBe(16)
  })

  test('randomInteger', () => {
    const result = randomInteger(0, 3)
    expect(result).toBeGreaterThanOrEqual(0)
    expect(result).toBeLessThanOrEqual(3)
  })

  test('randomElements', () => {
    const array = [0, 1, 2, 3, 4, 5]
    const count = randomInteger(1, array.length)
    const result = randomElements(array, count)
    expect(result.length).toBe(count)
  })
})
