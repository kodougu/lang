import { wait } from '@/lang/wait'
import { enhance, LOG_ENV_PREFIX } from '@/logger'
import { BrowserLogger } from '@/logger/browser/browserLogger'
import { Logger } from '@/logger/logger'

const browserLogger: Logger = new BrowserLogger('browser', LOG_ENV_PREFIX)
const logger: Logger = new BrowserLogger('default', LOG_ENV_PREFIX)
const accessLogger: Logger = new BrowserLogger('access', LOG_ENV_PREFIX)
const databaseLogger: Logger = new BrowserLogger('database', LOG_ENV_PREFIX)
const gitlabLogger: Logger = new BrowserLogger('gitlab', LOG_ENV_PREFIX)

afterAll(async () => {
  await wait()
})

describe('browserLogger.test', () => {
  test('logger#trace', () => {
    browserLogger.trace(enhance('trace'))
    logger.trace(enhance('trace'))
  })
  test('logger#debug', () => {
    browserLogger.debug('debug')
    logger.debug(enhance('debug'))
  })
  test('logger#info', () => {
    browserLogger.info('info')
    logger.info(enhance('info'))
  })
  test('logger#warn', () => {
    browserLogger.warn('warn')
    logger.warn(enhance('warn'))
  })
  test('logger#error', () => {
    browserLogger.error('error')
    logger.error(enhance('error'))
  })
  test('logger#fatal', () => {
    browserLogger.fatal('fatal')
    for (let i = 0; i < 20; i++) {
      browserLogger.fatal(enhance('fatal-' + i))
    }
    logger.fatal(enhance('fatal'))
  })
  test('accessLogger#fatal', () => {
    accessLogger.fatal(['1', '2', '3'])
  })
  test('accessLogger#debug', () => {
    accessLogger.debug({
      id: '123',
      name: 'abc',
    })
  })
  test('databaseLogger#info', () => {
    databaseLogger.info(
      enhance({
        id: '123',
        name: 'abc',
      }),
    )
  })
  test('gitlabLogger#f', () => {
    gitlabLogger.f('d', ['a', 'b', 'c'])
  })
  test('logger - no error', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const object: any = {}
    object.arr = [object, object]
    object.arr.push(object.arr)
    object.obj = object
    browserLogger.info(object)
  })
  test('logger - null', () => {
    browserLogger.info(null)
  })
  test('logger - undefined', () => {
    browserLogger.info(undefined)
  })

  test('logger#t', () => {
    browserLogger.t('trace', browserLogger.enhance({ hoge: 'fuga' }))
    logger.t('trace', browserLogger.enhance({ hoge: 'fuga' }))
  })
  test('logger#d', () => {
    browserLogger.d('debug', { hoge: 'fuga' })
    logger.d('debug', { hoge: 'fuga' })
  })
  test('logger#u', () => {
    browserLogger.i('info', { hoge: 'fuga' })
    logger.i('info', { hoge: 'fuga' })
  })
  test('logger#w', () => {
    browserLogger.w('warn', { hoge: 'fuga' })
    logger.w('warn', { hoge: 'fuga' })
  })
  test('logger#e', () => {
    browserLogger.e(browserLogger.enhance('error'), { hoge: 'fuga' })
    logger.e(browserLogger.enhance('error'), { hoge: 'fuga' })
  })
  test('logger#f', () => {
    browserLogger.f(browserLogger.enhance('fatal'), { hoge: 'fuga' })
    logger.f(browserLogger.enhance('fatal'), { hoge: 'fuga' })
  })
  test('callsites', () => {
    try {
      throw new Error('this is test')
    } catch (e) {
      browserLogger.e(browserLogger.enhance('test error!'), e)
    }
  })

  test('check enable', () => {
    expect(browserLogger.isTraceEnable()).toBeFalsy()
    expect(databaseLogger.isTraceEnable()).toBeFalsy()
    expect(accessLogger.isTraceEnable()).toBeFalsy()
    expect(gitlabLogger.isTraceEnable()).toBeFalsy()

    expect(browserLogger.isDebugEnable()).toBeFalsy()
    expect(databaseLogger.isDebugEnable()).toBeTruthy()
    expect(accessLogger.isDebugEnable()).toBeFalsy()
    expect(gitlabLogger.isDebugEnable()).toBeFalsy()

    expect(browserLogger.isInfoEnable()).toBeFalsy()
    expect(databaseLogger.isInfoEnable()).toBeTruthy()
    expect(accessLogger.isInfoEnable()).toBeTruthy()
    expect(gitlabLogger.isInfoEnable()).toBeFalsy()

    expect(browserLogger.isWarnEnable()).toBeTruthy()
    expect(databaseLogger.isWarnEnable()).toBeTruthy()
    expect(accessLogger.isWarnEnable()).toBeTruthy()
    expect(gitlabLogger.isWarnEnable()).toBeTruthy()
  })
})
