import { wait } from '@/lang/wait'
import { enhance, getLogger } from '@/logger'
import { Logger } from '@/logger/logger'

afterAll(() => {
  wait()
})

function log(env: string): Logger {
  process.env.NODE_ENV = env
  return getLogger(env)
}
function outInfo(env: string): void {
  log(env).info(enhance(`nodeEnv === ${env}`))
}
function outWarn(env: string): void {
  log(env).warn(enhance(`nodeEnv === ${env}`))
}

describe('node env', () => {
  test('nodeEnv === test', () => {
    const logger: Logger = getLogger('test')
    logger.warn(enhance('nodeEnv === test'))
  })
  test('nodeEnv === development', () => {
    outInfo('development')
  })
  test('nodeEnv === production', () => {
    outInfo('production')
  })
  test('nodeEnv === hoge', () => {
    process.env.LOG_LEVEL_HOGE = 'ERROR'
    outWarn('hoge')
  })
  test('nodeEnv === fuga', () => {
    process.env.LOG_LEVEL_FUGA = 'FATAL'
    outWarn('fuga')
  })
})
