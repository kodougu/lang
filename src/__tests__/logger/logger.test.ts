import { wait } from '@/lang/wait'
import { enhance, getLogger, logger } from '@/logger'

const accessLogger = getLogger('access')
const databaseLogger = getLogger('database')
const gitlabLogger = getLogger('gitlab')

afterAll(() => {
  wait()
})

describe('logger', () => {
  test('logger#trace', () => {
    logger.trace(enhance('trace'))
  })
  test('logger#debug', () => {
    logger.debug('debug')
  })
  test('logger#info', () => {
    logger.info('info')
  })
  test('logger#warn', () => {
    logger.warn('warn')
  })
  test('logger#error', () => {
    logger.error('error')
  })
  test('logger#fatal', () => {
    logger.fatal('fatal')
    for (let i = 0; i < 20; i++) {
      logger.fatal(enhance('fatal-' + i))
    }
  })
  test('accessLogger#fatal', () => {
    accessLogger.fatal(['1', '2', '3'])
  })
  test('accessLogger#debug', () => {
    accessLogger.debug({
      id: '123',
      name: 'abc',
    })
  })
  test('databaseLogger#info', () => {
    databaseLogger.info(
      enhance({
        id: '123',
        name: 'abc',
      }),
    )
  })
  test('gitlabLogger#f', () => {
    gitlabLogger.f('d', ['a', 'b', 'c'])
  })
  test('logger - no error', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const object: any = {}
    object.arr = [object, object]
    object.arr.push(object.arr)
    object.obj = object
    logger.info(object)
  })
  test('logger - null', () => {
    logger.info(null)
  })
  test('logger - undefined', () => {
    logger.info(undefined)
  })

  test('logger#t', () => {
    logger.t('trace', enhance({ hoge: 'fuga' }))
  })
  test('logger#d', () => {
    logger.d('debug', { hoge: 'fuga' })
  })
  test('logger#u', () => {
    logger.i('info', { hoge: 'fuga' })
  })
  test('logger#w', () => {
    logger.w('warn', { hoge: 'fuga' })
  })
  test('logger#e', () => {
    logger.e(enhance('error'), { hoge: 'fuga' })
  })
  test('logger#f', () => {
    logger.f(enhance('fatal'), { hoge: 'fuga' })
  })
  test('callsites', () => {
    try {
      throw new Error('this is test')
    } catch (e) {
      logger.e(enhance('test error!'), e)
    }
  })
  test('check enable', () => {
    expect(logger.isTraceEnable()).toBeTruthy()
    expect(databaseLogger.isTraceEnable()).toBeFalsy()
    expect(accessLogger.isTraceEnable()).toBeFalsy()
    expect(gitlabLogger.isTraceEnable()).toBeFalsy()

    expect(logger.isDebugEnable()).toBeTruthy()
    expect(databaseLogger.isDebugEnable()).toBeTruthy()
    expect(accessLogger.isDebugEnable()).toBeFalsy()
    expect(gitlabLogger.isDebugEnable()).toBeFalsy()

    expect(logger.isInfoEnable()).toBeTruthy()
    expect(databaseLogger.isInfoEnable()).toBeTruthy()
    expect(accessLogger.isInfoEnable()).toBeTruthy()
    expect(gitlabLogger.isInfoEnable()).toBeFalsy()

    expect(logger.isWarnEnable()).toBeTruthy()
    expect(databaseLogger.isWarnEnable()).toBeTruthy()
    expect(accessLogger.isWarnEnable()).toBeTruthy()
    expect(gitlabLogger.isWarnEnable()).toBeTruthy()
  })
})
