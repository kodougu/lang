import { LEVEL } from '@/logger/logLevel'

function getLevel(level: string | undefined): LEVEL {
  if (level === 'TRACE') return 'TRACE'
  if (level === 'DEBUG') return 'DEBUG'
  if (level === 'INFO') return 'INFO'
  if (level === 'WARN') return 'WARN'
  if (level === 'ERROR') return 'ERROR'
  if (level === 'FATAL') return 'FATAL'
  if (level === 'OFF') return 'OFF'
  return 'OFF'
}

export abstract class Logger {
  dateTimeFormat = 'YYYY-MM-DDTHH:mm:ss.SSS.Z'
  category = ''

  init(category: string, logEnvPrefix: string): void {
    this.category = category
    const logLevelEnvKey = logEnvPrefix + category.toUpperCase()
    if (process && process.env) {
      if (process.env[logLevelEnvKey]) {
        const level = getLevel(process.env[logLevelEnvKey])
        this.setLevel(level)
      } else if (process.env.NODE_ENV) {
        const mode = process.env.NODE_ENV
        if (mode === 'development') {
          this.setLevel('INFO')
        } else if (mode === 'test') {
          this.setLevel('WARN')
        } else if (mode === 'production') {
          this.setLevel('WARN')
        } else {
          this.setLevel('WARN')
        }
      }
    } else {
      this.setLevel('WARN')
    }
  }

  constructor(category: string, logEnvPrefix: string) {
    this.init(category, logEnvPrefix)
  }

  abstract setLevel(level: LEVEL): void
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  abstract enhance(target: any): any

  /* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/no-explicit-any */
  log(target: any): void {
    // This is intentional
  }
  trace(target: any): void {
    // This is intentional
  }
  debug(target: any): void {
    // This is intentional
  }
  info(target: any): void {
    // This is intentional
  }
  warn(target: any): void {
    // This is intentional
  }
  error(target: any): void {
    // This is intentional
  }
  fatal(target: any): void {
    // This is intentional
  }
  t(title: string, target: any): void {
    // This is intentional
  }
  d(title: string, target: any): void {
    // This is intentional
  }
  i(title: string, target: any): void {
    // This is intentional
  }
  w(title: string, target: any): void {
    // This is intentional
  }
  e(title: string, target: any): void {
    // This is intentional
  }
  f(title: string, target: any): void {
    // This is intentional
  }
  /* eslint-enable @typescript-eslint/no-unused-vars */

  isTraceEnable(): boolean {
    return false
  }
  isDebugEnable(): boolean {
    return false
  }
  isInfoEnable(): boolean {
    return false
  }
  isWarnEnable(): boolean {
    return false
  }
  isErrorEnable(): boolean {
    return false
  }
  isFatalEnable(): boolean {
    return false
  }
}
