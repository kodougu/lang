import dayjs from 'dayjs'
import { stringify } from '@/lang/stringify'
import { isObject } from '@/lang/is'
import { Logger } from '@/logger/logger'
import { LEVEL } from '@/logger/logLevel'

const TRACE = 'color:gray;font-weight:normal;'
const DEBUG = 'color:lightgreen;font-weight:normal;'
const INFO = 'color:skyblue;font-weight:normal;'
const WARN = 'color:orange;font-weight:bold;'
const ERROR = 'color:red;font-weight:bold;'
const FATAL = 'color:darkviolet;font-weight:bold;'

const LINE_FORMAT = '%c%s%s'
const DETAIL_FORMAT = '%c%s'

export class BrowserLogger extends Logger {
  constructor(category: string, logEnvPrefix: string) {
    super(category, logEnvPrefix)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  enhance(target: any): any {
    let msg = `${dayjs().format(super.dateTimeFormat)}`
    if (target instanceof Error) {
      msg += target.stack
    } else if (isObject(target)) {
      msg += stringify(target)
    } else {
      if (target !== undefined) {
        msg += target
      }
    }
    return msg
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private groupOutput(title: string, target: any, level: LEVEL): void {
    switch (level) {
      case 'TRACE':
        console.groupCollapsed(LINE_FORMAT, TRACE, '[TRACE]', title)
        console.log(DETAIL_FORMAT, TRACE, target)
        console.groupEnd()
        break
      case 'DEBUG':
        console.groupCollapsed(LINE_FORMAT, DEBUG, '[DEBUG]', title)
        console.log(DETAIL_FORMAT, DEBUG, target)
        console.groupEnd()
        break
      case 'INFO':
        console.groupCollapsed(LINE_FORMAT, INFO, '[INFO]', title)
        console.info(DETAIL_FORMAT, INFO, target)
        console.groupEnd()
        break
      case 'WARN':
        console.groupCollapsed(LINE_FORMAT, WARN, '[WARN]', title)
        console.warn(DETAIL_FORMAT, WARN, target)
        console.groupEnd()
        break
      case 'ERROR':
        console.groupCollapsed(LINE_FORMAT, ERROR, '[ERROR]', title)
        console.error(DETAIL_FORMAT, ERROR, target)
        console.groupEnd()
        break
      case 'FATAL':
        console.groupCollapsed(LINE_FORMAT, FATAL, '[FATAL]', title)
        console.error(DETAIL_FORMAT, FATAL, target)
        console.groupEnd()
        break
      case 'OFF':
        return
    }
  }

  setLevel(level: LEVEL): void {
    if (process.env.LOG_INIT_MESSAGE === 'true') {
      console.log(`LOG LEVEL: ${level}, LOG_CATEGORY: ${this.category}`)
    }
    switch (level) {
      case 'TRACE':
        this.trace = console.log.bind(console, LINE_FORMAT, TRACE, '[TRACE]')
        this.log = console.log.bind(console, LINE_FORMAT, TRACE)
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.t = (title: string, target: any): void => {
          this.groupOutput(title, target, 'TRACE')
        }
        this.isTraceEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'DEBUG':
        this.debug = console.log.bind(console, LINE_FORMAT, DEBUG, '[DEBUG]')
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.d = (title: string, target: any): void => {
          this.groupOutput(title, target, 'DEBUG')
        }
        this.isDebugEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'INFO':
        this.info = console.info.bind(console, LINE_FORMAT, INFO, '[INFO]')
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.i = (title: string, target: any): void => {
          this.groupOutput(title, target, 'INFO')
        }
        this.isInfoEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'WARN':
        this.warn = console.warn.bind(console, LINE_FORMAT, WARN, '[WARN]')
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.w = (title: string, target: any): void => {
          this.groupOutput(title, target, 'WARN')
        }
        this.isWarnEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'ERROR':
        this.error = console.error.bind(console, LINE_FORMAT, ERROR, '[ERROR]')
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.e = (title: string, target: any): void => {
          this.groupOutput(title, target, 'ERROR')
        }
        this.isErrorEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'FATAL':
        this.fatal = console.error.bind(console, LINE_FORMAT, FATAL, '[FATAL]')
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        this.f = (title: string, target: any): void => {
          this.groupOutput(title, target, 'FATAL')
        }
        this.isFatalEnable = (): boolean => {
          return true
        }
    }
  }
}
