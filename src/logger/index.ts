import * as dotenv from 'dotenv'
dotenv.config()

import { NodeJsLogger } from './nodejs/nodejsLogger'
import { BrowserLogger } from './browser/browserLogger'
import { Logger } from './logger'

let isNode = false
if (process && process.versions && process.versions.node) {
  isNode = true
}

// VUEのように、環境変数のプレフィックスが決まっている場合は、logEnvPrefixを「VUE_APP_LOG_LEVEL_」などに変更する
// defaultのloggerの環境変数の読み込みを後から変更する場合は、logger.init()を実行する
export const LOG_ENV_PREFIX = 'LOG_LEVEL_'

function newInstance(category: string, logEnvPrefix: string): Logger {
  return isNode ? new NodeJsLogger(category, logEnvPrefix) : new BrowserLogger(category, logEnvPrefix)
}

export const logger: Logger = newInstance('default', LOG_ENV_PREFIX)

const loggerMap = new Map<string, Logger>()
loggerMap.set('default', logger)

export function getLogger(category: string, logEnvPrefix = LOG_ENV_PREFIX): Logger {
  if (loggerMap.has(category)) {
    const l = loggerMap.get(category)
    return l ? l : logger
  } else {
    return newInstance(category, logEnvPrefix)
  }
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function enhance(target: any): any {
  return logger.enhance(target)
}
