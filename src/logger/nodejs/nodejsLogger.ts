import dayjs from 'dayjs'
import { stringify } from '@/lang/stringify'
import { isObject } from '@/lang/is'
import { getEnvByBoolean } from '@/lang/getEnv'
import { Logger } from '@/logger/logger'
import { LEVEL } from '@/logger/logLevel'

const TRACE = '\u001b[36m' // trace / log
const DEBUG = '\u001b[32m' // debug
const INFO = '\u001b[34m' // info
const WARN = '\u001b[33m' // warn
const ERROR = '\u001b[31m' // error
const FATAL = '\u001b[35m' // fatal
const ENHANCE = '\u001b[1;37m'
const RESET = '\u001b[0m'

const F = '%s%s%s%s%s'

export class NodeJsLogger extends Logger {
  lineNumber = false
  ignoreCallFrom = false

  init(category: string, logEnvPrefix: string): void {
    super.init(category, logEnvPrefix)
    // this option is used only NodeJsLogger
    const callFromEnvKey = 'LOG_IGNORE_CALL_FROM_' + category.toUpperCase()
    this.ignoreCallFrom = getEnvByBoolean(callFromEnvKey)
    const outputLineNumberEnvKey = 'LOG_OUTPUT_LINE_NUMBER_' + category.toUpperCase()
    this.lineNumber = getEnvByBoolean(outputLineNumberEnvKey)
  }

  constructor(category: string, logEnvPrefix: string) {
    super(category, logEnvPrefix)
    this.init(category, logEnvPrefix)
  }

  private parseLine(
    stack: string,
  ): {
    fileName: string
    lineNumber: string
    columnNumber: string
  } {
    const line = stack.substring(stack.lastIndexOf('(') + 1, stack.lastIndexOf(')'))
    const elements = line.split(':')
    const result = {
      fileName: elements[0],
      lineNumber: elements[1],
      columnNumber: elements[2],
    }
    const fileName = result.fileName
    if (fileName.startsWith(process.cwd())) {
      result.fileName = fileName.substr(process.cwd().length + 1)
    }
    return result
  }

  private getCallFrom(): string {
    let msg = ''
    if (this.ignoreCallFrom) {
      return msg
    }
    const stack = new Error().stack
    let idx = -1
    if (stack) {
      const stacks = stack.split('\n')
      for (let i = 0; i < stacks.length; i++) {
        const line = stacks[i].trim()
        if (line.indexOf('nodejsLogger') !== -1) {
          idx = i + 3
          break
        }
      }
      if (idx !== -1 && idx < stacks.length) {
        const parsed = this.parseLine(stacks[idx])
        msg += ` ${parsed.fileName}`
        if (this.lineNumber) {
          msg += `:${parsed.lineNumber}`
        }
      }
    }
    return msg
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/no-explicit-any
  enhance(target: any): string {
    let msg = `${ENHANCE}${dayjs().format(super.dateTimeFormat)}`
    msg += this.getCallFrom()
    msg += ` ${RESET}`

    if (target instanceof Error) {
      msg += target.stack
    } else if (isObject(target)) {
      msg += stringify(target)
    } else {
      msg += target
    }
    return msg
  }

  setLevel(level: LEVEL): void {
    if (process.env.LOG_INIT_MESSAGE === 'true') {
      console.log(`LOG LEVEL: ${level}, LOG_CATEGORY: ${this.category}`)
    }
    switch (level) {
      case 'TRACE':
        this.trace = console.log.bind(console, F, TRACE, '[TRACE] ', `-${this.category}- `, RESET)
        this.log = console.log.bind(console, F, TRACE, '[LOG] ', `-${this.category}- `, RESET)
        this.t = console.log.bind(console, F, TRACE, '[TRACE] ', `-${this.category}- `, RESET)
        this.isTraceEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'DEBUG':
        this.debug = console.log.bind(console, F, DEBUG, '[DEBUG] ', `-${this.category}- `, RESET)
        this.d = console.log.bind(console, F, DEBUG, '[DEBUG] ', `-${this.category}- `, RESET)
        this.isDebugEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'INFO':
        this.info = console.info.bind(console, F, INFO, '[INFO] ', `-${this.category}- `, RESET)
        this.i = console.info.bind(console, F, INFO, '[INFO] ', `-${this.category}- `, RESET)
        this.isInfoEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'WARN':
        this.warn = console.warn.bind(console, F, WARN, '[WARN] ', `-${this.category}- `, RESET)
        this.w = console.warn.bind(console, F, WARN, '[WARN] ', `-${this.category}- `, RESET)
        this.isWarnEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'ERROR':
        this.error = console.error.bind(console, F, ERROR, '[ERROR] ', `-${this.category}- `, RESET)
        this.e = console.error.bind(console, F, ERROR, '[ERROR] ', `-${this.category}- `, RESET)
        this.isErrorEnable = (): boolean => {
          return true
        }
      /* falls through */
      case 'FATAL':
        this.fatal = console.error.bind(console, F, FATAL, '[FATAL] ', `-${this.category}- `, RESET)
        this.f = console.error.bind(console, F, FATAL, '[FATAL] ', `-${this.category}- `, RESET)
        this.isFatalEnable = (): boolean => {
          return true
        }
    }
  }
}
