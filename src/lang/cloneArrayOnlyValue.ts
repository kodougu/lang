import { cloneOnlyValue, Resource } from '@/lang/cloneOnlyValue'

/**
 * 指定のオブジェクトの値のみをコピーして新しいインスタンスを生成する
 * 値： boolean, number, string のみ
 * @param obj
 */
export function cloneArrayOnlyValue<T extends Resource>(arr: T[]): T[] {
  const results: T[] = []
  arr.forEach((i) => {
    results.push(cloneOnlyValue(i))
  })
  return results
}
