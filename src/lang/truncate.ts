export function truncate(value: string, length: number | string, omission: string): string {
  let len = undefined
  if (typeof length === 'string') {
    len = parseInt(length, 10)
  } else {
    len = length
  }
  const ommision = omission ? omission.toString() : '...'
  if (value.length <= length) {
    return value
  }
  return value.substring(0, len) + ommision
}
