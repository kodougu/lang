export function mask(secret: string, maskChar = '*'): string {
  return secret
    .split('')
    .map(() => maskChar)
    .join('')
}
