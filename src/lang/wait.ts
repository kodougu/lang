export async function wait(term = 100): Promise<void> {
  return new Promise<void>((resolve) => {
    setTimeout(() => {
      return resolve()
    }, term)
  })
}
