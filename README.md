# lang

- utilities library module

## How to development

- create .env from .sample.env

```
$ cp .sample.env .env && \
  echo "CI_JOB_TOKEN=" >> .env && \
  echo "CI_PROJECT_ID=15718754" >> .env
```

- install dependencies libraries to your environment

```
$ docker-compose run --rm node yarn install --ignore-optional
```

- Lint

```
$ docker-compose run --rm node yarn run lint
$ docker-compose run --rm node yarn run lint:fix
```

- Test / Coverage

```
$ docker-compose run --rm node yarn run test
$ docker-compose run --rm node yarn run test:coverage
```

- SonarQube

```
$ docker-compose -f docker-compose-sonarqube.yml run --rm sonar-scanner
```


- build

```
$ docker-compose run --rm node yarn run build
```


## Tips

- If you want use VSC(Visual Studio Code) code, terminal in visual studio code will output error message at startup.
- You can fix this error message if you add this setting to VSC

- Error

```
Error: Failed to replace env in config: ${CI_JOB_TOKEN}
    at /Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/lib/config/core.js:415:13
    at String.replace (<anonymous>)
    at envReplace (/Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/lib/config/core.js:411:12)
    at parseField (/Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/lib/config/core.js:389:7)
    at /Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/lib/config/core.js:330:24
    at Array.forEach (<anonymous>)
    at Conf.add (/Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/lib/config/core.js:328:23)
    at ConfigChain.addString (/Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/node_modules/config-chain/index.js:244:8)
    at Conf.<anonymous> (/Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/lib/config/core.js:316:10)
    at /Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/node_modules/graceful-fs/graceful-fs.js:90:16
/Users/koda/.nvm/versions/node/v10.16.0/lib/node_modules/npm/lib/npm.js:59
      throw new Error('npm.load() required')
```

- Setting to VSC

```
{
    "settings": {
        "terminal.integrated.env.osx": {
            "CI_JOB_TOKEN": "",
            "CI_PROJECT_ID": "PROJECT"
        }
    }
}
```
